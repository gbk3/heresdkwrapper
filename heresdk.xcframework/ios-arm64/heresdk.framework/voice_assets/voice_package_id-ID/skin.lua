_package_settings = {}
main_attribute_array = {}
voice_settings = {}

language = "Indonesian"
language_id = "21"
language_loc = "Bahasa Indonesia"
main_attribute_array["LocalizedType"] = "suara sintesis"
main_attribute_array["language_code"] = "id-ID"
main_attribute_array["ngLangCode"] = "ind"
marc_code = "IND"
speed_camera = "true"
speed_warner = "true"

_package_settings["NuanceLanguagePriorities"] = {'GER', 'UKE', 'IDE', 'IRE', 'SAE', 'SEN', 'ENG', 'THE', 'GJE'}
application_support_list = { 'IN "NUANCE_TTS_Core":1.0' }
audio_files_path = "resources"
audio_files_version = "0.5.0.202109301724"
client_range = "[ client >= 4.1.0.0 ]"
config_file = "main.lua"
configurable = "true"
description = "voice package for "..language_loc
engine_id = "01"
feature_list = { "metric", "imperial_uk", "imperial_us" }
id = language_id..marc_code
main_attribute_array["VoiceFeatures"] = "drive;walk;metric;imperialuk;imperialus;naturalguidance;trafficlights;tts"
network_provider_support_list = "all"
output_type = "tts"
platform_support_list = { '="HERE SDK for Android":>= 1.0' }
rulesets_file = "rulesets.txt"
travel_mode = "0"
tts_engine_type = { "nuance:5.5" }
userdictionary_file = ""
voice_settings["nguidance"] = "true"
voice_settings["nguidance_junction"] = "true"
voice_settings["nguidance_stop_sign"] = "true"
voice_settings["nguidance_trafficlights"] = "true"
voice_settings["olympia_prompt_mode"] = "1"
