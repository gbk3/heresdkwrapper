local mods = require('translations.mods')

local two_block_char_distance = 28 --in UTF, 2-block characters are placed every 28 characters (there are
                                   --27 3-block characters between every 2-bloch characters
local hangul_utf_offset = 44032
local hangul_utf_end_offset = 55125

--Korean characters are represented by 3 bytes in UTF, but certain bits are "reserved"
--|1110xxxx|10xxxxxx|10xxxxxx|
-- In order to get UTF value, from three bytes encoding the charcter, we need to:
-- - perform AND operation on the oldest byte with 00001111 (dec 15) and shift it 12 bits
-- - perform AND operation on middle byte with 00111111 (dec 63) and shift it 6 bits
-- - perform AND operation on youngest byte with 00111111 (dec 63) and shift it 6 bits
-- - sum all the above
-- Since we can't use bitwise operators, due to Lua compatibility reasons, we'll use equivalent operations
-- in base 10 system. From oldest byte we subtract 224, to "zero" 4 oldest bits, and multiplied by 4096, to
-- bit-shift the whole number by 12 bits. For the middle byte we subtract 128, to "zero" two oldes bits, and
-- multiply by 64, to shift 6 bits. From youngest byte we just subtract 128.
-- Now, from result we must subtract 0xAC00 (dec. 44032), which is offset in UTF, where Korean characters begin
-- Hangul characters are placed in UTF in such a way, that first, there is put 2-block character, and after
-- that all 3-block characters, that can be built from this 2-block character. Since there is 27 possibilities,
-- new 2-block characters are 28 values apart, so if our code is difisible by 28 without a reminder, we know,
-- that we have 2-block character, and 3-block one otherwise
function is_last_char_3block(utf_char)
    utf_char = utf_char - hangul_utf_offset
    return (utf_char % two_block_char_distance ~= 0)
end
--table containig vowels
local vowels = {"a", "A", "@", "*", "e", "E", "i", "I", "o", "O", "u", "U", "^"}
--Table containig consonants, that can be attached to an existing syllable, and create a 3-block character
local attachable_consonants = {"p", "t", "k", "l", "m", "n"}

--function checks if last sound of a phoneme is one of attachable_consonants and sound preceding it
--is a vowel. If so, such combination would be rendered as a 3-block Hangul character in Korean wtiting
--Otherwise 2-block Hangul would be rendered
function determine_blocknum_for_end_phoneme(word)
    for _, consonant in ipairs(attachable_consonants) do
        if (word:sub(-1) == consonant) then
            for _, vowel in ipairs(vowels) do
                if (word:sub(-2, -2) == vowel) then
                    return 3
                end
            end
        end
    end
    return 2
end


local oldest_utf_byte_offset = 224 --First byte of 3-byte UTF character starts with 1110xxxx, to get
                                   --the actual value of the byte we need to subtract 11100000 (bin)
                                   --from it, that is 224 (dec)
local younger_utf_utf_byte_offset = 128 --Second and third bytes in 3-byte UTF character start with
                                        --10xxxxxx, to get their actual values we need to subtract
                                        --10000000 (bin), that is 128 (dec)
local oldest_utf_byte_shift = 4096 --Most signifficant byte of (3-byte) UTF character encodes 4 oldest 
                                   --bits of UTF value we need to shift it 12 bits, which is equivalend to 
                                   --multiplying by 4096 (for compatibility with older Lua versions, we do it 
                                   --in decimal)
local middle_utf_byte_shift = 64 --Least signifficant byte of (3-byte) utf character encodes youngest 6 bits of
                                 --UTF value, middle byte needs to be shifted 6 bits, which is equivalent
                                 --of multiplying it's value by 64 (dec)
local escape_ascii_value = 27

--function determines, whether Hangul character ends with a 2-block character or 3-block character
-- and returns 2 or 3 respectively, returns 0 for non hangul character
function determine_blocknum_for_end_char(word)
    local oldest_utf_byte = string.byte((word:sub(-3,-3)))
    oldest_utf_byte = oldest_utf_byte - oldest_utf_byte_offset
    if( oldest_utf_byte >= 0 and oldest_utf_byte <= 0xF ) then --To check if it is a 3-byte UTF char
        local middle_utf_byte = string.byte((word:sub(-2,-2)))
        local youngest_utf_byte = string.byte((word:sub(-1)))
        local utf_char = ((oldest_utf_byte * oldest_utf_byte_shift)+((middle_utf_byte - younger_utf_utf_byte_offset) * middle_utf_byte_shift)+(youngest_utf_byte -younger_utf_utf_byte_offset))
        -- Hangul characters are placed in UTF range of 0xAC00 (dec. 44032) to 0xD7AF (dec 55125)
        if (utf_char >= hangul_utf_offset and utf_char <= hangul_utf_end_offset ) then
            if is_last_char_3block(utf_char) then
                return 3
            else
                return 2
            end
        end
    else --If not, maybe we extract phonemes from the word?
        local phoneme = word:gsub(".*toi=nts:(.+)\\(.+)\\toi=orth.*", "%2")
        local last_word = phoneme:gsub(".*\"([^\"]*)$", "%1")
        last_word = last_word:gsub("(.*)<ESC>", "%1")
        if (string.byte(last_word:sub(-1)) == escape_ascii_value ) then
            last_word = last_word:sub(1, -2)
        end
        return determine_blocknum_for_end_phoneme(last_word)
    end
    return 0
end

--if a character preceding "(으)로" construct is written using three block Hangul syllable, then we need
--to return "으로" suffix, if it is a two block syllable, then we return "로" suffix
function suffix_matcher_to_in(word, _)
    local numblocks = determine_blocknum_for_end_char(word)
    if (numblocks == 3) then
        return word.." 으로"
    elseif (numblocks == 2 ) then
        return word.." 로"
    end
    return word
end

--을 particle should be placed for words ending with 3-block Hangul characters, and 를 for 2-block characters
function suffix_matcher_of_to(first_part, word, last_part)
    local numblocks = determine_blocknum_for_end_char(word)
    if (numblocks == 3) then
        return first_part.."\xB8"..word.."을\xB8"..last_part
    elseif (numblocks == 2 ) then
        return first_part.."\xB8"..word.."를\xB8"..last_part
    end
    return first_part.."\xB8"..word.."을/를".."\xB8"..last_part
end

--Functions reorders words in certain cases, to sound more naturally in Korean. Since some words
--are part of e.g. DIRECTION clause, and are not part of a template, we need to do in modding function
function reorder_direction(turn, direction)
    local result = string.gsub(direction, "\xB8(.+)\xB8 \xB7(.*)\xB7(.*)", "%1 \xB8" .. turn .. "\xB8 %2")
    result = string.gsub(result, "(.*)\xB8(.+)\xB8(.*)", suffix_matcher_of_to)
    if (result == nil or result == '' or result == direction)then
        return turn.." "..direction
    end
    return result
end

--Functions reorders words in certain cases, to sound more naturally in Korean. Since some words
--are part of e.g. DIRECTION clause, and are not part of a template, we need to do in modding function
function reorder_direction_exit(turn, constant_part ,direction)
    direction = string.gsub(direction, "(.*)\xB8(.+)\xB8(.*)", suffix_matcher_of_to)
    local result = string.gsub(direction, "\xB8(.+)\xB8 \xB7(.+)\xB7(.+)", "%2 "..turn..constant_part.."%1")
    if (result == nil or result == '')then
        return turn..constant_part..direction
    end
    return result
end

--This functions searches if variable containing no entity (wchich is an object of the action) is
--inserted to a template sentence. If such case is found (marked as "\xAE" in variables.lua), particle
--should be changed to "앞에서", othwewise "앞" should be left.
function discover_entity(first_part, particle, terminal_part)
    if (terminal_part:find("\xAE") ~= nil) then
        particle = "앞에서"
        terminal_part = terminal_part:gsub("(.*)\xAE(.*)", "%1%2")
    end
    return first_part..particle..terminal_part
end

--If exit name is just a number, "번" should be appended to it.
local function is_exit_a_number(first_part, exit_value, last_part)
    if (exit_value ~= nil or exit_value ~= "") then
        if (tonumber(exit_value) ~= nil) then
            return first_part..exit_value.."번"..last_part
        end
    end
    return first_part..exit_value..last_part
end

--In some cases, word "exit" (나가십시오) should be removed.
local function remove_exit_word(sentence, terminal_part)
    local cut_out_sentence = sentence:gsub("(.*)\xB0(.*)\xB0(.*)", "%1%3")
    return cut_out_sentence..terminal_part
end

local function reorder_marked_sentence(sentence)
    local retval = string.gsub(sentence, "십시오를 따라", "다가")
    if (retval ~= nil and retval ~= "") then
        return retval
    end
    return sentence
end

local function reorder_next_direction(first_part, direction, end_part)
    direction = string.gsub(direction, "(.*) 로", "%1로")
    local retval = string.gsub(first_part, "(.*)차로로 주행하십시오(.*)", "%1차로를 따라 "..direction.." 주행하십시오%2")
    return retval..end_part
end

function mods.apply_language_specific_mods(sentence)
    sentence = string.gsub (sentence, "\x2023(.+)\x2023(%(으%)로)", suffix_matcher_to_in)
    sentence = string.gsub (sentence, "(.*)\x20AB(.*)\x20AB(.*)", reorder_next_direction)
    sentence = string.gsub (sentence, "(.*)\xAF(.+)\xAF(.*)", discover_entity)
    sentence = string.gsub (sentence, "\xAB(.+)\xAB \xA6(.+)\xA6", reorder_direction)
    sentence = string.gsub (sentence, "\xAB(.+)\xAB(.+)\xA6(.+)\xA6", reorder_direction_exit)
    sentence = string.gsub (sentence, "(.*)\xB8(.+)\xB8 \xB7(.+)\xB7(.*)", "%1%2 %3%4")
    sentence = string.gsub (sentence, "(.*)\x25E6(.*)\x25E6(.*)", is_exit_a_number)
    sentence = string.gsub (sentence, "(.*)\x2022(.*)", remove_exit_word)
    sentence = string.gsub (sentence, "(.*)\xB8(.*)\xB8(.*)", "%1%2%3")
    sentence = string.gsub (sentence, "(.*)\xB0(.*)\xB0(.*)", "%1%2%3")
    sentence = string.gsub (sentence, "(.*)\x20AA", reorder_marked_sentence)
    sentence = string.gsub (sentence, "(.*)\xAE(.*)", "%1%2")
    sentence = string.gsub (sentence, "(.*)xAE(.*)", "%1%2")
    return sentence
end

return mods
