local mods = require('translations.mods')

--change the word meaning "turn" to "keep" for commands like "keep left"
local function make_keep(first_part, last_part)
    last_part = string.gsub(last_part, "(.*)पर लें(.*)", "%1पर रहें%2")
    return first_part..last_part
end

--change the particle, when we have "keep side" and not "turn side"
local function particle_change_for_keepside(first_part, direction, last_part)
    last_part = string.gsub(last_part, "(.*)मुड़ें।(.*)", "%1रहें।%2")
    if ((string.find(last_part, "\xB7") ~= nil) and (string.find(last_part, "तुरंत") == nil)) then
        direction = string.gsub(direction, "बाईं ओर", "बाएं")
        direction = string.gsub(direction, "दाईं ओर", "दाएं")
        first_part = string.gsub(first_part, "(.*),(.*)", "%1%2")
    end
    last_part = string.gsub(last_part, "(.*)\xB7", "%1")
    return first_part..direction..last_part
end

local function particle_change_for_landmark(sentence)
    local firstpart, lastpart = string.match(sentence, "(.*)\xAF(.*)")
    lastpart = string.gsub(lastpart, "(.*)पर लें(.*)", "%1पर मुड़ें%2")
    return firstpart..lastpart
end

local function add_use_suffix(sentence)
    if ((string.find(sentence, "इस्तेमाल") ~= nil) and (string.find(sentence, "इस्तेमाल कर") == nil)) then
        sentence = string.gsub(sentence, "(.*)इस्तेमाल(.*)", "%1इस्तेमाल कर%2")
    end
    return sentence
end

function mods.apply_language_specific_mods(sentence)
    sentence = string.gsub (sentence, "(.*)\xB8", particle_change_for_landmark)
    sentence = string.gsub (sentence, "(.*)\xAF(.*)", make_keep)
    sentence = string.gsub (sentence, "(.*)\xB6", add_use_suffix)
    sentence = string.gsub (sentence, "(.*)\xBF(.*)\xBF(.*)", particle_change_for_keepside)
    sentence = string.gsub (sentence, "(.*)xAF(.*)", "%1%2")
    sentence = string.gsub (sentence, "(.*)xB7(.*)", "%1%2")
    sentence = string.gsub (sentence, "(.*)xB6(.*)", "%1%2")
    sentence = string.gsub (sentence, "(.*)xBF(.*)xBF(.*)", "%1%2%3")
    sentence = string.gsub (sentence, "(.*)ओर ओर(.*)की ओर (.*) रहें।", "%1 %2की ओर %3 रहें।")
    sentence = string.gsub (sentence, "(.*)ईं ओर मुड़ें(.*)", "%1ईं ओर रहें%2")
    sentence = string.gsub (sentence, "(.*)ओर ओर(.*)", "%1ओर%2")
    sentence = string.gsub (sentence, "(.*)की ओर दाईं ओर रहें।", "%1की ओर दाएं रहें।")
    sentence = string.gsub (sentence, "(.*)की ओर बाईं ओर रहें।", "%1की ओर बाएं रहें।")
    return sentence
end

return mods
