// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "HereSDKWrapper",
  platforms: [
    .iOS("14")
  ],
  products: [
    .library(
      name: "HereSDKWrapper",
      targets: ["heresdk"]),
    .library(
      name: "heresdk",
      targets: ["heresdk"])
  ],
  dependencies: [
  ],
  targets: [
    .target(
      name: "HereSDKWrapper",
      dependencies: [
        .target(name: "heresdk")
      ]),
    .binaryTarget(
      name: "heresdk",
      path: "heresdk.xcframework"
    ),
    .testTarget(
      name: "HereSDKWrapperTests",
      dependencies: ["HereSDKWrapper"]),
  ]
)
